package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/command"
	"gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/plugin"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "NodeJsScan analyzer for GitLab SAST"
	app.Author = "GitLab"

	app.Commands = []cli.Command{
		Run(),
		command.Search(plugin.Match),
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
