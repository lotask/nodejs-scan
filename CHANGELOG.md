# NodeJsScan analyzer changelog

GitLab TODO analyzer follows versioning of GitLab (`MAJOR.MINOR` only) and generates a `MAJOR-MINOR-stable` [Docker image](https://gitlab.com/gitlab-org/security-products/nodejs-scan/container_registry).

These "stable" Docker images may be updated after release date, changes are added to the corresponding section bellow.

## 11-1-stable
- Initial releas
