package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/search"
	"gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/plugin"
)

const (
	artifactName        = "gl-sast-report.json"
	flagTargetDir       = "target-dir"
	flagArtifactDir     = "artifact-dir"
	flagRulesPath       = "rules"
	pathClosureCompiler = "/closure-compiler.jar"
	toolID              = "nodejs_scan"
)

// Run is inspired from command.Run of the analyzers/common library.
func Run() cli.Command {
	flags := []cli.Flag{
		cli.StringFlag{
			Name:   flagTargetDir,
			Usage:  "Target directory",
			EnvVar: "ANALYZER_TARGET_DIR,CI_PROJECT_DIR",
		},
		cli.StringFlag{
			Name:   flagArtifactDir,
			Usage:  "Artifact directory",
			EnvVar: "ANALYZER_ARTIFACT_DIR,CI_PROJECT_DIR",
		},
		cli.StringFlag{
			Name:   flagRulesPath,
			Usage:  "Path of NodeJsScan XMLrules",
			EnvVar: "NODEJS_SCAN_RULES_FILE",
			Value:  "rules.xml",
		},
	}
	flags = append(flags, search.NewFlags()...)

	return cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a SAST artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errInvalidArgs
			}

			// search directory
			root, err := filepath.Abs(c.String(flagTargetDir))
			root = strings.TrimSuffix(root, "/") + "/"
			if err != nil {
				return err
			}

			// search
			searchOpts := search.NewOptions(c)
			matchPath, err := search.New(plugin.Match, searchOpts).Run(root)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, "Found project in "+matchPath)

			// load rules
			pathRules := c.String(flagRulesPath)
			rules, err := loadRules(pathRules)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, len(rules), "rules loaded") // DEBUG

			// build files list
			paths := []string{}
			relpaths := []string{}
			err = filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if !info.IsDir() {
					if ok, _ := plugin.Match(path, info); ok {
						paths = append(paths, path)
						relpaths = append(relpaths, strings.Replace(path, root, "", 1))
					}
				}
				return nil
			})
			if err != nil {
				return err
			}

			// remove comments using closure-compile
			// and concatenates all source files into a single file
			args := []string{
				"-jar", pathClosureCompiler,
				"--compilation_level", "WHITESPACE_ONLY",
				"--formatting", "PRETTY_PRINT",
				"--formatting", "PRINT_INPUT_DELIMITER",
				"--warning_level", "QUIET",
				"--language_in", "ECMASCRIPT5",
			}
			args = append(args, paths...)

			cmd := exec.Command("java", args...)
			cmd.Env = os.Environ()
			cmd.Stderr = os.Stderr

			// TODO use Cmd.StdoutPipe to reduce memory footprint
			// See https://golang.org/pkg/os/exec/#Cmd.StdoutPipe
			output, err := cmd.Output()
			if err != nil {
				return err
			}

			// apply rules to find issues
			fileSep := regexp.MustCompile(`// Input (\d+)`)
			reader := bytes.NewReader(output)
			issues := []issue.Issue{}
			scanner := bufio.NewScanner(reader)
			var relpath string
			var lineNumber int
			for scanner.Scan() {
				line := scanner.Text()

				// look for file header
				if m := fileSep.FindStringSubmatch(line); m != nil {
					idx, err := strconv.Atoi(m[1])
					if err != nil {
						return err
					}
					relpath = relpaths[idx]
					fmt.Fprintln(c.App.Writer, "Checking", relpath) // DEBUG
					lineNumber = 0
					continue
				}

				// process line
				lineNumber++
				for _, rule := range rules {
					if rule.Match(line) {
						def := rule.Definition()
						issues = append(issues, issue.Issue{
							Tool:        toolID,
							Category:    issue.CategorySast,
							Name:        def.Name,
							Message:     def.Name,
							Description: def.Description,
							Location: issue.Location{
								File:      relpath,
								LineStart: lineNumber,
							},
							CompareKey: def.Name + ":" + line,
						})
					}
				}
			}

			// write indented JSON to artifact
			artifactPath := filepath.Join(c.String(flagArtifactDir), artifactName)
			f2, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f2.Close()
			enc := json.NewEncoder(f2)
			enc.SetIndent("", "  ")
			return enc.Encode(issues)
		},
	}
}

func loadRules(path string) ([]Rule, error) {
	set := RuleSet{}
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	err = xml.NewDecoder(f).Decode(&set)
	return set.Rules()
}
