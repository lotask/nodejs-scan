FROM  openjdk:8-jdk-alpine

ARG NODEJS_SCAN_VERSION
ARG CLOSURE_COMPILER_VERSION

ENV NODEJS_SCAN_VERSION ${NODEJS_SCAN_VERSION:-2.5}
ENV CLOSURE_COMPILER_VERSION ${CLOSURE_COMPILER_VERSION:-20180610}

ADD https://raw.githubusercontent.com/ajinabraham/NodeJsScan/v${NODEJS_SCAN_VERSION}/core/rules.xml /
ADD https://dl.google.com/closure-compiler/compiler-${CLOSURE_COMPILER_VERSION}.tar.gz /
RUN tar -zxf compiler-${CLOSURE_COMPILER_VERSION}.tar.gz && mv /closure-compiler-v${CLOSURE_COMPILER_VERSION}.jar /closure-compiler.jar
COPY analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]
